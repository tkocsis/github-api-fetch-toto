* legyen egy olyan komponens, ami egy in-memory db-t valósít meg, és user-eket, issue-kat tud tárolni, és keresni név alapján
* készíts egy olyan komponenst,
    * ami a github api-ról le tudja kérni a paraméterben megadott user public repo-it
    * le tudja kérni egy paraméterben megadott repo-hoz tartozó issue-kat
* az in-memory db legyen thread safe, és legyen erre letesztelve unit-teszttel
* a fejlesztés a kapott issue-k alapján történik
* a git csak command line használható
