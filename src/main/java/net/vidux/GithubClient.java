package net.vidux;

import java.io.*;
import java.net.*;

public class GithubClient {

		/*public void gitConnection() throws IOException {
		URL myURL = new URL("https://github.com/");
		URLConnection myURLConnection = myURL.openConnection();
		myURLConnection.connect();
		}*/
		
		public String urlConnectionUserReader( String inputLine) throws IOException {

	        URL oracle = new URL("https://api.github.com/users/"+inputLine);
	        BufferedReader in = new BufferedReader(
	        new InputStreamReader(oracle.openStream()));

	        while ((inputLine = in.readLine()) != null)
	            System.out.println(inputLine);
	        in.close();
			return inputLine;
		}
		
		public String urlConnectionReposReader( String inputLine) throws IOException {

	        URL oracle = new URL("https://api.github.com/users/"+inputLine+"/repos");
	        BufferedReader in = new BufferedReader(
	        new InputStreamReader(oracle.openStream()));

	        while ((inputLine = in.readLine()) != null)
	            System.out.println(inputLine);
	        in.close();
			return inputLine;
		}
	
}
/*public String executeCommand(String command) {
StringBuffer output = new StringBuffer();
Process p;
try {
	p=Runtime.getRuntime().exec(command);
	BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
		String line = "";
	while((line = reader.readLine())!=null) {
		output.append(line+"\n");
		}
}catch(Exception e) {
	e.printStackTrace();
}
return output.toString();

}*/
