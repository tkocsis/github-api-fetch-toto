package net.vidux;


public class User{

	String login;
	int id = 0;
	String nodeId;
	String url;
	String reposUrl;
	Type type;
	int publicrepos = 0;
		
	public User(String login, int id, String nodeId, String url, String reposUrl, Type type, int publicrepos) {
		this.login = login;
		this.id = id; 
	}

	public String getlogin() {
		return login;
	}
	
	public void setLogin(String login) {
		this.login = login;
	}
	
	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public int getId () {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNodeId() {
		return nodeId;
	}

	public void setNodeId(String nodeId) {
		this.nodeId = nodeId;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getReposUrl() {
		return reposUrl;
	}

	public void setReposUrl(String reposUrl) {
		this.reposUrl = reposUrl;
	}

	public int getPublicRepos() {
		return publicrepos;
	}
	public void setPublicRepos (int publicrepos) {
		this.publicrepos = publicrepos;
	}
		
	@Override
	public String toString() {
		return "User: "+System.lineSeparator() +  
					 "login = " + login +","  + System.lineSeparator() + 
					 "id = " + id +"," + System.lineSeparator() + 
					 "nodeId = " + nodeId +","  + System.lineSeparator() + 
					 "url = " + url +","  + System.lineSeparator() + 
					 "reposUrl = " + reposUrl +","  + System.lineSeparator() + 
					 "type = " + type;
	}





}
