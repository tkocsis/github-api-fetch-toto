package net.vidux;

import java.sql.*;

public class Database {
	   private static Database instance = null;
	   private Database() { }
	   private Connection conn = null; 
	   private Statement stmt = null;
	   private final String JDBC_DRIVER = "org.h2.Driver";   
	   private final String DB_URL = "jdbc:h2:mem:vidux";  
	   
	   //  Database credentials 
	   final String USER = "toto"; 
	   final String PASS = "root"; 
	   
	   public static Database instance() {
		   if(instance == null) {
			   instance = new Database();
		   }
		   return instance;
	   }
	   	   
	   public void openConnection() throws SQLException, ClassNotFoundException {
		   Class.forName(JDBC_DRIVER); 	             
	       System.out.println("Adatbázishoz kapcsolodás..."); 
	       conn = DriverManager.getConnection(DB_URL,USER,PASS);
	   }
	   
	   public void dbCreate() throws ClassNotFoundException, SQLException {
		         System.out.println("Táblák létrehozása a megadott adatbázisban..."); 
		         stmt = conn.createStatement(); 
		         String sql =  "CREATE TABLE   USER " + 
		            "(id INTEGER not NULL, " + 
		            " login VARCHAR(255), " +  
		            " nodeId VARCHAR(255), " + 
		            " url VARCHAR(255), " +
		            " reposUrl VARCHAR(255), " +
		            " type ENUM('USER','ADMIN'), "+
		            " publicrepos INTEGER, " +  
		            " PRIMARY KEY ( id ));";
		         stmt.executeUpdate(sql);
		         System.out.println("A USER tábla létre lett hozva...");
		         
		         String dwk = "CREATE TABLE ISSUE " + 
				            "(id INTEGER not NULL, " +  
				            " nodeId VARCHAR(255), " +
				            " number INTEGER, " + 
				            " title VARCHAR(255), " +
				            " userlogin VARCHAR (255), " +
				            " state VARCHAR(255), " + 
				            " PRIMARY KEY ( id ));";
		         stmt.executeUpdate(dwk);
		         System.out.println("Az ISSUE tábla létre lett hozva...");
		         
		         /*String gre = "CREATE TABLE ENTITY "+
		        		 	  "(id INTEGER not NULL, "+
		        		 	  "login VARCHAR(255));";
		         stmt.execute(gre);
		         System.out.println("Az ENTITY tábla létre lett hozva...");
		         
		         System.out.println("Link létrehozása a táblák között...");
		         String fel = "ALTER TABLE ISSUE" +
		        		 	  "	ADD FOREIGN KEY (userlogin)" +
		        		 	  "	REFERENCES USER (login)";
		         stmt.executeUpdate(fel);
		         System.out.println("Link létre lett hozva...");*/
		        		 
	   }
	   
       public void addUser(String login, int id, String nodeId, String url, String reposUrl, String type, int publicrepos) throws SQLException {
        	   System.out.println("User létrehozása"); 
        	   stmt = conn.createStatement();
        	   String sql =  "INSERT INTO USER (id, login, nodeId, url, reposUrl, type, publicrepos)"
        	   				+" VALUES "+"("+"'"+id+"'"+","+"'"+login+"'"+","+"'"+nodeId+"'"+","+"'"+url+"'"+","+"'"+reposUrl+"'"+","+"'"+type+"'"+","+"'"+publicrepos+"'"+")"+";";    
    		         stmt.executeUpdate(sql);
    		         System.out.println("User: "+login+" létre lett hozva...");	
       }
       
       public void deleteUser(String login) throws SQLException {
    	   System.out.println("User törlése");
    	   stmt = conn.createStatement();
    	   String sql = "DELETE FROM USER WHERE login = '"+login+"'";
    	   stmt.executeUpdate(sql);
    	   System.out.println("User: "+login+" kitörölve...");
       }
       
       public void searchUser(String user) throws SQLException {
    	   System.out.println(user+" keresése...");
    	   stmt = conn.createStatement();
    	   String sql = "SELECT * FROM USER WHERE login ='"+user+"'";
    	   ResultSet rs = stmt.executeQuery(sql);
    	   while(rs.next()) {
    		   int id = rs.getInt("id");
    		   String login = rs.getString("login");
    		   String nodeId = rs.getString("nodeId");
    		   String url = rs.getString("url");
    		   String reposUrl = rs.getString("reposUrl");
    		   short type = rs.getShort("type");
    		   int publicrepos = rs.getInt("publicrepos");
    		   System.out.println("ID: " + id);
    		   System.out.println("Login: " + login);
    		   System.out.println("NodeId: " + nodeId);
    		   System.out.println("Url: " + url);
    		   System.out.println("ReposUrl: " + reposUrl);
    		   System.out.println("Type: " + type);
    		   System.out.println("Publicrepos: " + publicrepos);
    	   }
    	   rs.close();
       }
       
       public void addIssue(int id, String nodeId, int number, String title, String userlogin, String state) throws SQLException {
    	   System.out.println("Issue létrehozása"); 
    	   stmt = conn.createStatement();
    	   String sql =  "INSERT INTO Issue (id, nodeId, number, title, userlogin, state)"
    	   				+" VALUES "+"("+"'"+id+"'"+","+"'"+nodeId+"'"+","+"'"+number+"'"+","+"'"+title+"'"+","+"'"+userlogin+"'"+","+"'"+state+"'"+")"+";";    
		         stmt.executeUpdate(sql);
		         System.out.println("Issue: "+number+" létre lett hozva...");	
       }
       
       public void deleteIssue(int number) throws SQLException {
    	   System.out.println("Issue törlése");
    	   stmt = conn.createStatement();
    	   String sql = "DELETE FROM ISSUE WHERE number = '"+number+"'";
    	   stmt.executeUpdate(sql);
    	   System.out.println("Issue: "+number+" kitörölve...");
       }
       
       public void searchIssue(int szam) throws SQLException {
    	   System.out.println("Issue "+szam+" keresése...");
    	   stmt = conn.createStatement();
    	   String sql = "SELECT * FROM ISSUE WHERE number ='"+szam+"'";
    	   ResultSet rs = stmt.executeQuery(sql);
    	   while(rs.next()) {
    		   int id = rs.getInt("id");
    		   String nodeId = rs.getString("nodeId");
    		   int number = rs.getInt("number");
    		   String title = rs.getString("title");
    		   String userlogin = rs.getString("userlogin");
    		   String state = rs.getString("state");
    		   System.out.println("ID: " + id);
    		   System.out.println("NodeId: " + nodeId);
    		   System.out.println("Number: " + number);
    		   System.out.println("Title: " + title);
    		   System.out.println("Userlogin: " + userlogin);
    		   System.out.println("State: " + state);
    	   }
    	   rs.close();
       }
       
       public void closeConnection() throws SQLException {
    	   System.out.println("Adatbázisrol lekapcsolodás..."); 
    	   conn.close();
       }
       
       public void finalize() {
    	   instance = null;
       }
}
