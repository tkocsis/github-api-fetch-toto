package net.vidux;

public class Issue {

	int id;
	String nodeId;
	int number;
	String title;
	User user;
	String state;
	
	
	public Issue(int id, String nodeId, int number, String title, User user, String state) {
		this.id = id;
		this.nodeId = nodeId;
		this.number = number;
		this.title = title;
		this.user = user;
		this.state = state;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNodeId() {
		return nodeId;
	}
	public void setNodeId(String nodeId) {
		this.nodeId = nodeId;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	
	@Override
	public String toString() {
		return "Issues: " + System.lineSeparator() +
						"[id = " + id + 
						",nodeId = " + nodeId + 
						",number = " + number + 
						",title = " + title +
						",state = " + state +
						",user = " + user + "]";
	}
}
