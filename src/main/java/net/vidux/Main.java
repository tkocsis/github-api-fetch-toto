package net.vidux;

import java.io.IOException;
import java.sql.SQLException;

public class Main {

	   // JDBC driver name and database URL 

		  
	public static void main(String[] args) throws SQLException, ClassNotFoundException, IOException {
		Database vidux = Database.instance();
		vidux.openConnection();
		vidux.dbCreate();
		vidux.addUser("toto",1,"nodeId","Url","reposUrl","ADMIN",1);
		vidux.addUser("Kevin",2,"nodeId","Url","reposUrl","USER",1);
		vidux.deleteUser("toto");
		vidux.addIssue(1, "nodeId", 1, "bugfix", "toto", "80%");
		vidux.addIssue(2, "nodeId", 2, "update", "Kevin", "30%");
		vidux.deleteIssue(2);
		vidux.searchUser("Kevin");
		vidux.searchIssue(1);
		vidux.closeConnection();
		
		System.out.println("");
		GithubClient git = new GithubClient();
		String gituser = git.urlConnectionUserReader("totototem");
		String gitrepos = git.urlConnectionReposReader("totototem");
		System.out.println(gituser);
		System.out.println(gitrepos);
		/*GithubClient obj = new GithubClient();
		String command="curl https://api.github.com/users/";
		String gitUser ="totototem";
		String task = command + gitUser;
		String output=obj.executeCommand(task);
		System.out.println(output);*/
				
		User user=new User(null, 0, null, null, null, null, 0);
		user.setLogin("tester");
		user.setNodeId("NodeId");
		user.setId(123);
		user.setReposUrl("valami");
		user.setType(Type.ADMIN);
		user.setUrl("valami");	
		user.setNodeId("NodeId");
		user.setId(2);
		user.setReposUrl("valami");
		user.setType(Type.USER);
		user.setUrl("valami");
		
		Issue issues= new Issue(0, null, 0, null, null, null);
		issues.setId(1);
		issues.setNodeId("Node");
		issues.setNumber(123);
		issues.setState("state");
		issues.setTitle("title");
		issues.setUser(user);
		System.out.println("");
		System.out.println("Hello");
		System.out.println("");
		System.out.println(user  );
		System.out.println("");
		System.out.println(issues  );
		System.out.println("");


		}
}
		
